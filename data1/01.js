// 1. 使用require 方法 加载 fs 核心模块
var fs = require('fs')

// 2. 读取文件
// 读取的文件路径，回调函数，
fs.readFile('node_02.md',function (err, data) {
    // <Buffer > 68 65 6c 6c 6f
    // 二进制数据 0 1
    // 这里为什么看到的不是0 1 ? 原因二进制位16 进制了
    // 我们可以通过toString()

    // 如果失败
    if (err) {
        console.log('读取文件失败')
    } else {
        console.log(data.toString())
    }
})