var http = require('http');

var server = http.createServer()

 /**
  * request 请求事件处理函数，需要接受2个函数:
  *     request: 请求对象
  *         请求对象可以用来获取客户端的一些请求信息，例如请求路径
  *     response: 响应对象
  *         响应对象可以用来给客户端发送响应消息
  */

server.on('request', function (request, response) {
    console.log('收到客户端的请求了,请求路径是：' + request.url)

    // response 对象有一个方法, write 可以用来给客户发送响应数据
    // write 可以使用多次，但是最后一定要使用end来结束响应，否则客户端会一直等待
    response.write('hello')
    
    // 告诉客户端，我的话说完了，你可以呈递给用户了

    // 思考： 当请求不同路径时响应不同的结果
    if(request.url == '/index') {
        response.write('---index');
    } else if (request.url == '/login') {
        response.write('---login')
    } else if (request.url == '/register') {
        response.write('---register')
    } else if (request.url == '/cangku') {
        response.write('---cangku')
    } else {
        response.write('404 not Found')
    }

    response.end()

})

server.listen(3306, function () {
    console.log('服务器启动了， 可以通过 http://127.0.0.1:3306/ 来进行访问')
})