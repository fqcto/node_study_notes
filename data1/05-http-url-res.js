// 1. 引入http模块
var http = require('http')

// 2. 创建 server
var server = http.createServer()

// 3. 监听 request 请求事件， 设置请求处理函数
server.on('request', function (req, res) {
    console.log('收到请求了，请求路径是：' + req.url)
    console.log('请求我的客户端的地址是：' + req.socket.remoteAddress,req.socket.remotePort)

    // res.write('hello)
    // res.write(' world')
    // res.end()

    // 上面的方式比较麻烦，推荐使用更简单的方式，直接end 的同时带参数
    // res.end('hello nodejs')

    // 响应内容只能是二进制数据或者字符串
    // No -->  数字 - 对象 - 数组 - 布尔值
    var url = req.url
    if ( url === '/products') {
        var products = [
            {
                name: '苹果X',
                price: 8888
            },
            {
                name: '小米',
                price: 6666
            },
            {
                name: '华为',
                price: 9999
            },
            {
                name: '魅族',
                price: 4444
            }
        ]
        res.end(JSON.stringify(products))
    } else {
        res.end('404 not found')
    }
})

// 4. 绑定端口，启动服务
server.listen(3306, function () {
    console.log('服务器启动成功了， 可以通过 http://127.0.0.1:3306/ 来进行访问')
})