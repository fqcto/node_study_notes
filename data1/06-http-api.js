// 用来获取机器信息的
var os = require('os')

// 用来操作路径的
var path = require('path')

// 获取当前系统的cpu信息
console.log(os.cpus())

// memory 内存
console.log(os.totalmem())

// 获取一个路径中的扩展名部分
// extname extension name 
console.log(path.extname('c:/a/b/c/d/hello.txt'))

// 文件作用域/模块作用域，--互不干扰 -- 加载自定义模块，可以省略后缀名, './'不能省略-- require('./b')
/**
 *  在 node 中，
 * 没有全局作用域，
 * 只有模块作用域，
 * 外部访问不到内部，
 * 内部也访问不到外部
 *  默认都是封闭的
 * 既然是模块作用域， 那如何让模块与模块之间进行通信
 * 有时候，
 * 我们加载文件模块的目的不是为了简简单单的执行里面的代码，
 * 更重要是为了使用里面的函数方法
 * 
 * */
