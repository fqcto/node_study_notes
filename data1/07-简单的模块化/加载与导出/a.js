var fs = require('fs')
console.log('aaa')
var bExports = require('./b')

console.log(bExports.foo)
console.log(bExports.add(10,30))
console.log(bExports.age)
bExports.readFile('./a.js')

fs.readFile('./a.js', function (err,data) {
    if (err) {
        console.log('读取文件失败')
    } else {
        console.log(data.toString())
    }
})

/**
 * require 加载模块
 * var c = require('./c') <==> var c = require("./c.js")
 *  1. 加载文件模块并执行里面的代码
 *  2. 拿到被加载文件模块导出（exports）的接口对象
 * 
 *  在每个文件模块中都提供了一个对象:exports
 *  exports 默认是一个空对象
 *  你要做的就是把所有被外部访问的成员暴露出去
 */

