// app application 应用程序
// 把当前模块所有的依赖项度声明在文件模块最上面
// 为了让目录结构保持统一清晰，所以我们约定，把所有的HTML文件度放到views试图里。

var http = require('http')
var fs = require('fs')
var template = require('art-template')
var url = require('url')

var comments = [
  {
    name: '张三1',
    message:  '今天天气好',
    dateTime: '2020-4-3'
  },
  {
    name: '张三2',
    message:  '今天天气好',
    dateTime: '2020-4-4'
  },
  {
    name: '张三3',
    message:  '今天天气好',
    dateTime: '2020-4-5'
  },
  {
    name: '张三4',
    message:  '今天天气好',
    dateTime: '2020-4-6'
  },
  {
    name: '张三5',
    message:  '今天天气好',
    dateTime: '2020-4-7'
  }
]

/**
 * 对于这种表单提交的请求路径，由于其中具有用户动态填写的内容
 * 所以你不可能通过去判断完整的url路径来处理这个请求
 * 
 * 结论：对于我们来讲，其实只需要判定，如果你的请求路径是 /pinglun 的时候，
 * 那我就可以用url模块获取路径和query参数
 * 
 */

http
  .createServer(function (req,res) {
    // 使用 url.parse 方法将路径解析为一个方便操作的对象，第二个参数为true表示直接
    let parseObj = url.parse(req.url,true)
    
    // 单独获取不包含查询字符串的路径部分
    let pathname = parseObj.pathname
    if (pathname === '/') {
      fs.readFile('./views/index.html', function (err, data) {
        if (err) {
          return res.end('404 not found.')
        } 
        var htmlStr = template.render(data.toString(),{
          comments:comments
        })
        res.end(htmlStr)
      })
    } else if (pathname === '/pinglun') {
      // 注意： /pinglun?xxx 后面不管是什么度会进来
      // 一次请求对应一次响应 ， 响应结束
      // res.end(JSON.stringify(parseObj.query))
      //  我们已经使用url模块的parse方法把请求路径中的查询字符串给解析成了一个对象
      /**
       * 接下来：
       *  1.获取表单提交的数据 parseObj.query
       *  2.生成日期到数据对象中，然后存储到数组中
       *  3.让用户重定向跳转到首页  /
       *    当页面再刷新时，数据已经是变化后的状态了
       */
      let comment = parseObj.query
      comment.dateTime = '2020-4-9'
      comments.push(comment)
      // 服务端这个时候已经吧数据存储好了，接下来就是让用户重新请求/首页，就可以看到增加的数据的了
      // 如何通过服务器让客户端重定向?
      //  1. 状态码设置为 302 临时重定向
      //      statusCode
      //  2. 在响应头中通过 Location 告诉客户端往哪儿重定向
      //      setHeader
      //  3. 如何客户端发现收到服务器的响应的状态码是302 就会自动去响应头中找Location
      //  4. 所以你就能看到客户端自动跳转了
      
      res.statusCode = 302
      res.setHeader('Location', '/')
      res.end()

    } else if (pathname === '/post') {
      fs.readFile('./views/post.html',function (err, data) {
        if(err){
          return res.end('404 not found')
        }
        res.end(data)
      })
    } else if (pathname.indexOf('/public/') === 0)
    // /public/css/main.css
    // /public/js/main.js
    // /public/lib/jquery.js
    // 统一处理:
    //    如果请求路径是以 /public/ 开头的，则我认为你要获取 public 中的某个资源
    //    所以我们就直接可以把请求路径当做文件路径来直接进行读取
    {
      fs.readFile('.' + pathname, function (err, data) {
        if (err) {
          return res.end('404 not found.')
        }
        res.end(data)
      })
    } else {
      // 其他的度处理成 404 找不到
      fs.readFile('./views/404.html',function (err, data) {
        if(err) {
          return res.end('404 not found.')
        }
        res.end(data)
      })
    }
  })
  .listen(3000, function () {
    console.log('running...')
  })