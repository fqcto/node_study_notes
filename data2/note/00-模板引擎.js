var template = require('art-template')
var fs = require('fs')
// 这里不是浏览器
// template('script 标签 id', {对象})

// var tplStr = `
// <!DOCTYPE html>
// <html lang="en">
// <head>
//   <meta charset="UTF-8">
//   <meta name="viewport" content="width=device-width, initial-scale=1.0">
//   <title>Document</title>
// </head>
// <body>
//   <P>大家好，{{ name }}</P>
// </body>
// </html>
// `


fs.readFile('./tpl.html', function (err, data) {
  if (err) {
    return console.log('读取文件失败')
  }
  // 默认读取的 data 是二进制数据
  // 而模板引擎的 render 方法需要接受的是字符串
  // 所以我们在这里需要把 data 二进制数据转为 字符串 才可以给模板引擎使用
  var ret = template.render(data.toString(), {
    name: 'Jack'
  })
  console.log(ret)
})

