// 在NODE中每一个模块内部都有一个自己的module对象
// 该module 对象中，有一个成员叫： exports 

var module = {
  exports: {
    foo: 'bar'
  }
}

exports.foo = 'bar'

// 谁来 require 我，谁就得到 module.exports
// 默认在代码的最后有一句：
// return module.exports