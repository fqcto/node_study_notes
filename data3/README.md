# 6.2.1 加载 require
> 语法
>> var 自定义变量名称 = require('模块')

<!-- 两个作用
  执行被加载模块中的代码
  得到被加载模块中的 exports 导出接口对象
 -->

# 6.2.2 导出 exports
``` 
  Node 中是模块作用域，默认文件中所有的成员只在当前文件模块有效
  对于希望可以被其他模块访问的成员，我们就需要把这些公开的成员都挂载到 exports 接口对象中就可以了,导出多个成员（必须在对象中）
 ```
```
  exports.a = 123
  exports.b = 'hello'
  exports.c = function () {
    console.log('ccc)
  }
  exports.d = {
    foo:'foo'
  }
```
导出单个成员 （拿到的就是，函数，字符串）

```
  module.exports = 'hello'

  module.exports = function (x, y) {
    return x + y
  }

  module.exports = {
    add: function () {
      return x + y
    },
    str: 'hello'
  }
    // 后者exports赋值会覆盖前者
```

